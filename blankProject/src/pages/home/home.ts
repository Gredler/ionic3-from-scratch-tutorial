import { RegisterPage } from './../register/register';
import { LoginPage } from './../login/login';
import { FirstPage } from './../first/first';
import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController
  ) {}

  openFirstPage() {
    this.navCtrl.push(FirstPage);
  }

  signIn() {
    this.navCtrl.push(LoginPage);
  }

  register() {
    this.navCtrl.push(RegisterPage);
  }
}
