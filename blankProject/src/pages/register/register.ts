import { LoggedinPage } from './../loggedin/loggedin';
import { Component, ViewChild } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  @ViewChild('username') user;
  @ViewChild('password') password;

  constructor(
    private fire: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registerUser() {
    this.fire.auth
      .createUserWithEmailAndPassword(this.user.value, this.password.value)
      .then(data => {
        console.log('got data: ', data);
        this.alert('Success! You\'re registered');
        this.navCtrl.setRoot( LoggedinPage );
      })
      .catch(error => {
        console.log('error: ', error);
        this.alert(error.message);
      });
    console.log(
      'Registering User ' + this.user.value + ' - ' + this.password.value
    );
  }

  alert(message: string) {
    this.alertCtrl
      .create({
        title: 'Info!',
        subTitle: message,
        buttons: ['ok']
      })
      .present();
  }
}
